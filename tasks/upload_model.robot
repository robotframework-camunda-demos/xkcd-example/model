*** Settings ***
Library    CamundaLibrary    ${CAMUNDA_HOST}
Library    OperatingSystem


*** Variables ***
${CAMUNDA_HOST}    %{CAMUNDA_HOST}
${PATH_TO_MODEL}    ${CURDIR}/../bpmn/robot-suite.bpmn


*** Tasks ***
Upload model to Camunda
    File Should Exist    ${PATH_TO_MODEL}
    ${response}    Deploy    ${PATH_TO_MODEL}
    Log    ${response}
